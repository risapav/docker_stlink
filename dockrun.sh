#!/bin/sh

docker run -h stlink_host   \
           --rm         \
           --privileged \
	   -p 4242:4242 \
           -v /dev/bus/usb:/dev/bus/usb      \
	   -v $PWD:/project                  \
	   -w /project                       \
           -it risapav/docker_stlink
