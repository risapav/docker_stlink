# Používa konkrétnu verziu Alpine na zabezpečenie konzistentnosti
FROM alpine:3.20 

# Inštaluje iba potrebné balíky a minimalizuje veľkosť obrazu
RUN apk add --no-cache stlink && \
# Vytvára neprivilegovaného používateľa na zvýšenie bezpečnosti
    adduser -D -u 1000 appuser

USER appuser

# Definuje vystavený port (voliteľné)
EXPOSE 4242 

# Nastaví predvolený príkaz
CMD ["/bin/sh"]
