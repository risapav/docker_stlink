#!/bin/sh

set -ex

for image in stlink
do
   echo "\nbuilding $image"
   docker build \
     -t "risapav/docker_$image" \
     . -f Dockerfile
done
